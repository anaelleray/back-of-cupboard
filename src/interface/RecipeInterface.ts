/**
 * Recipe Interface -> Récapitulatif de toutes les méthodes exécutables du Controller (action) Recipe 
 */
import { Recipe } from "../entity/Recipe";
import { Ingredient } from "../entity/Ingredient";

export interface RecipeInterface {

    createNewRecipe(recipe: Recipe):void;
    fetchIngredients(ingredients:Ingredient):void;

}