/**
 * Ingredient Interface -> Récapitulatif de toutes les méthodes exécutables du Controller (action) Ingredient 
 */
import { Ingredient } from "../entity/Ingredient";

export interface IngredientInterface {
    
    stockIngredient(type:Ingredient):void;
    createNewIngredient(type:Ingredient):void;
}