"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Recipe = void 0;
var Recipe = /** @class */ (function () {
    function Recipe(name, category, image, score, ingredient) {
        this.name = name;
        this.category = category;
        this.image = image;
        this.score = score;
        this.ingredient = ingredient;
    }
    Recipe.prototype.getName = function () {
        return this.name;
    };
    Recipe.prototype.getCategory = function () {
        return this.category;
    };
    Recipe.prototype.getImage = function () {
        return this.image;
    };
    Recipe.prototype.getScore = function () {
        return this.score;
    };
    return Recipe;
}());
exports.Recipe = Recipe;
