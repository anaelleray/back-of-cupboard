/**
 * Entité Ingredient
 */

export class Ingredient {
    constructor(
        private idIng:string,
        private name: string,
    ) { }

    getId():string{
        return this.idIng
    }
    getName(): string {
        return this.name;
    }
}

