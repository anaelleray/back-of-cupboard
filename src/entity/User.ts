export class User {
    constructor(
        private name:string,
        private email:string,
        private accessRecipe:boolean
    ){}
    getUserName():string{
        return this.name;
    }
    getUserEmail():string{
        return this.email
    }
    getUserAccess():boolean{
        return this.accessRecipe
    }
}