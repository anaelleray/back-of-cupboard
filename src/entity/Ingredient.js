"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ingredient = void 0;
var Ingredient = /** @class */ (function () {
    function Ingredient(name, recipe) {
        this.name = name;
        this.recipe = recipe;
    }
    Ingredient.prototype.getName = function () {
        return this.name;
    };
    Ingredient.prototype.assignToRecipe = function (recipe) {
        this.recipe = recipe;
        this.recipe.setRecipe(name);
    };
    return Ingredient;
}());
exports.Ingredient = Ingredient;
