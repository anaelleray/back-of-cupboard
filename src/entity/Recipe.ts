/**
 * Entité Recipe
 */
import { IngredientInterface } from "../interface/IngredientInterface";

export class Recipe {
    //private refIngredient:IngredientInterface;

    constructor(
        private idRe:string,
        private name: string,
        private category: string,
        private image: string,
        private score: number,
        //method target : stockIngredient
        private ingredients: IngredientInterface[]
        
    ) { }
    getIdRecipe():string{
        return this.idRe
    }
    getName(): string {
        return this.name
    }
    getCategory(): string {
        return this.category
    }
    getImage(): string {
        return this.image
    }
    getScore(): number {
        return this.score
    }
    getIngredients():IngredientInterface[]{
        return this.ingredients
    }
}
