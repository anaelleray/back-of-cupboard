import firebase from 'firebase/app';
import 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'
import 'firebase/firebase-functions'

var firebaseConfig = {
    apiKey: "AIzaSyBd3mj2TEWmuRtKMnk7tOC8HZwRSFDpfjM",
    authDomain: "back-of-cupboard.firebaseapp.com",
    databaseURL: "https://back-of-cupboard.firebaseio.com",
    projectId: "back-of-cupboard",
    storageBucket: "back-of-cupboard.appspot.com",
    messagingSenderId: "944297249952",
    appId: "1:944297249952:web:d4ebc37c63690728ee8dc5"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;