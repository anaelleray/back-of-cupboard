/**
 * Ingredient actions (controller) -> défini toutes les méthodes de Ingredient
 */
import { Ingredient } from "../entity/Ingredient";
import { Recipe } from "../entity/Recipe";
import { IngredientInterface } from "../interface/IngredientInterface";

var admin = require("firebase-admin");
var serviceAccount = require('../../functions/serviceKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://back-of-cupboard.firebaseio.com",
});
const db = admin.firestore()

class IngredientActions implements IngredientInterface {

    stockIngredient(type:Ingredient) {
        let arrayIngredient
    
        db.collection('Ingredient').get().then(function (querySnapshot: any) {

            querySnapshot.forEach(function (doc: any) {

                let data = doc.data()
                console.log('DATA ->', data);
                let idIng = doc.id;
                arrayIngredient = [idIng, data];
            
            })

        }).catch(function (error: any) {
            console.log(error);
        })
        return arrayIngredient;
    }

    createNewIngredient(type:Ingredient){
        // let newObject = {
        // }

        db.collection('Ingredient').get().then(function (querySnapshot: any) {
            querySnapshot.forEach(async function (doc: any) {
                let idIng = doc.id;
                //Méthode pour auto incrémenter l'Id en base si il est indéfini (ce qui sera toujours le cas)
                if(idIng === undefined && idIng === null){
        
                    let maxId = -1;
                    const idSnap = await db.ref('Ingredient').once('value')
                    idSnap.forEach(async function(){
                        const index = parseInt(idIng.replace('I', ''))

                        if (maxId === -1 || maxId < index) {
                            maxId = index
                            maxId++
                        } else {
                            console.log('Impossible de créer un nouvel Id');
                            debugger
                        }

                        //await db.Collection('Ingredient').doc(`${idIng}`).set(data, {merge:true})
                
                    })
                }
            })
        })

    }
}
