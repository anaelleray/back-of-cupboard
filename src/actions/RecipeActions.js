"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Recipe_1 = require("../entity/Recipe");
var RecipeActions = /** @class */ (function () {
    function RecipeActions() {
    }
    RecipeActions.newRecipe = function (type) {
        var name = "";
        var category = "";
        var image = "";
        var score = 0;
        var ingredient = [];
        var newRecipe = new Recipe_1.Recipe(name, category, image, score, ingredient, []);
    };
    return RecipeActions;
}());
