/**
 * Recipe actions (controller) -> défini toutes les méthodes de Recipe
 */
import { Ingredient } from "../entity/Ingredient";
import { Recipe } from "../entity/Recipe";
import { IngredientInterface } from "../interface/IngredientInterface";
import { RecipeInterface } from "../interface/RecipeInterface";

//Initialisation de la base pour récupérer les données
var admin = require("firebase-admin");
var serviceAccount = require('../../functions/serviceKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://back-of-cupboard.firebaseio.com",
});
const db = admin.firestore()
//

class RecipeActions implements RecipeInterface {
    protected ingredientmethod!: IngredientInterface;

    fetchIngredients(ingredients: Ingredient) {
        this.ingredientmethod.stockIngredient(ingredients)
    }

    createNewRecipe(recipe: Recipe) {
        //Object test
        // let formRecipeResult:Recipe = {
        //     idRe : '',
        //     name : '',
        //     category: '',
        //     image: '',
        //     score: 0,
        //     ingredients: []
        // }
        // const data = {formRecipeResult}

        db.collection('Ingredient').get().then(function (querySnapshot: any) {
            querySnapshot.forEach(async function (doc: any) {
                let idRe = doc.id;
                //Méthode pour auto incrémenter l'Id en base si il est indéfini (ce qui sera toujours le cas)
                if (idRe === undefined && idRe === null) {
                    let maxId = -1;
                    const idSnap = await db.ref('Ingredient').once('value')
                    idSnap.forEach(async function () {
                        const index = parseInt(idRe.replace('I', ''))

                        if (maxId === -1 || maxId < index) {
                            maxId = index
                            maxId++
                        } else {
                            console.log('Impossible de créer un nouvel Id');
                            debugger
                        }

                        //await db.Collection('Recipe').doc(`${idRe}`).set(data, { merge: true })

                    })
                }
            })
        })

    }


}